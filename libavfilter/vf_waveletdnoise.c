/*
 * 2017 Martin Krutak xkruta03 (at) vutbr (dot) cz
 *
 * This file is part of FFmpeg.
 *
 * Disclaimer: pieces of code taken from libavfilter/vf_edgedetect.c
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * @file
 * Denoise video with wavelets
 *
 *
 */
#include "libavutil/qsort.h"
#include "libavutil/common.h"
#include "libavutil/imgutils.h"
#include "libavutil/avassert.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "avfilter.h"
#include "formats.h"
#include "internal.h"
#include "video.h"

//#include "drawutils.h" // padding

#define RMIN 2

typedef struct {
    const AVClass *class;
    int nb_planes;
} WaveletDenoiseContext;

#define OFFSET(x) offsetof(WaveletDenoiseContext, x)
#define FLAGS AV_OPT_FLAG_FILTERING_PARAM|AV_OPT_FLAG_VIDEO_PARAM
static const AVOption waveletdnoise_options[] = {
    { NULL }
};

AVFILTER_DEFINE_CLASS(waveletdnoise);

static int query_formats(AVFilterContext *ctx)
{
    static const enum AVPixelFormat pix_fmts[] = {
        AV_PIX_FMT_GBRP,  AV_PIX_FMT_GRAY8, AV_PIX_FMT_NONE
    };
    AVFilterFormats *fmts_list = ff_make_format_list(pix_fmts);
    if (!fmts_list)
        return AVERROR(ENOMEM);
    return ff_set_common_formats(ctx, fmts_list);
}

static int config_props(AVFilterLink *inlink)
{
    //AVFilterContext *ctx = inlink->dst;
    //WaveletDenoiseContext *waveletdnoise = ctx->priv;
    return 0;
}

static int comp (const void * elem1, const void * elem2)
{
    int f = *((int*)elem1);
    int s = *((int*)elem2);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

double *coeffs = NULL;

static double estimate_thr(
    int w,
    uint8_t *src, int src_linesize,
    int orig_h, int orig_w)
{
    int pArr, n, i, j, NR;
    double median, sigma, lambda;
    int coef_holder;
    NR = w;
    if (coeffs == NULL) coeffs = (double*)malloc(orig_w / 2 * orig_h / 2 * sizeof(double)); // av_malloc_array??
    pArr = 0;
    for (i = 0; i < orig_h / 2; i++)
    {
        for (j = w / 2; j < (w / 2 - orig_w / 2); ++i)
        {
            coef_holder = src[(w / 2 + i) * src_linesize + w / 2 + j];
            coeffs[pArr] = FFABS(coef_holder);
        }
    }
    n = sizeof (coeffs) / sizeof (*coeffs);    // Number of elements in an array
    AV_QSORT(coeffs, n, double, comp);
    median = coeffs[n / 2];
    sigma = median / 0.6745;
    lambda = sigma * sqrt(2 * log(NR * NR));
    return lambda;
}

static int find_size_to_fit_log(
    int w, int h)
{
    int finRes, width, height;
    width = 2;
    height = 2;
    // check if already power of two
    if ((w & (w - 1)) != 0) {
        while (w >>= 1) width <<= 1;
    }
    else {
        width = w;
    }
    if ((h & (h - 1)) != 0) {
        while (h >>= 1) height <<= 1;
    }
    else {
        height = h;
    }
    if ((height - h == 0) && (width - w == 0)) return -1; // no need to resize
    finRes = FFMAX(width, height);
    return finRes;
}

double *tbank = NULL;

// bi - orthogonal 9 / 7 wavelet transformation(lifting implementation)
// author: Gregoire Pau <gregoire.pau@ebi.ac.uk>
// length of row has to be the power of two
static void fast_wav_tr97(uint8_t *row, int nr_rows) {
    double p1_97 = -1.586134342;
    double u1_97 = -0.05298011854;
    double p2_97 = 0.8829110762;
    double u2_97 = 0.4435068522;
    double scale = 1. / 1.149604398;
    int i;

    //tbank = NULL;
    // phase 1 predict
    for (i = 1; i < nr_rows - 2; i += 2)
    {
        row[i] += p1_97 * (row[i - 1] + row[i + 1]);
    }
    row[nr_rows - 1] += 2 * p1_97 * row[nr_rows - 2];

    // phase 1 update
    for (i = 2; i < nr_rows; i += 2)
    {
        row[i] += u1_97 * (row[i - 1] + row[i + 1]);
    }
    row[0] += 2 * u1_97 * row[1];

    // phase 2 predict
    for (i = 1; i < nr_rows - 2; i += 2)
    {
        row[i] += p2_97 * (row[i - 1] + row[i + 1]);
    }
    row[nr_rows - 1] += 2 * p2_97 * row[nr_rows - 2];

    // phase 2 update
    for (i = 2; i < nr_rows; i += 2)
    {
        row[i] += u2_97 * (row[i - 1] + row[i + 1]);
    }
    row[0] += 2 * u2_97 * row[1];

    // scale
    for (i = 0; i < nr_rows; i++)
    {
        if (i % 2 != 0) {
            row[i] *= scale;
        }
        else {
            row[i] /= scale;
        }
    }

    // pack
    if (tbank == NULL) tbank = (double*)malloc(nr_rows * sizeof(double));
    for (i = 0; i < nr_rows; i++)
    {
        if (i % 2 == 0) {
            tbank[i / 2] = row[i];
        }
        else {
            tbank[nr_rows / 2 + i / 2] = row[i];
        }
    }
    memcpy(row, tbank, nr_rows);
}

static void iwd_wav_tr97(uint8_t *row, int nr_rows) {
    double ip1_97 = 1.586134342;
    double iu1_97 = 0.05298011854;
    double ip2_97 = -0.8829110762;
    double iu2_97 = -0.4435068522;
    double iscale = 1.149604398;

    int i;
    // unpack
    if (tbank == NULL) tbank = (double*)malloc(nr_rows * sizeof(double));
    for (i = 0; i < (nr_rows >> 1); i++)
    {
        tbank[i * 2]   = row[i];
        tbank[i * 2 + 1] = row[i + nr_rows / 2];
    }
    memcpy(row, tbank, nr_rows);
    // undo scale
    for (i = 0; i < nr_rows; i++)
    {
        if (i % 2 != 0) {
            row[i] *= iscale;
        }
        else {
            row[i] /= iscale;
        }
    }

    // undo phase 2 update
    for (i = 2; i < nr_rows; i += 2)
    {
        row[i] += iu2_97 * (row[i - 1] + row[i + 1]);
    }
    row[0] += 2 * iu2_97 * row[1];

    // undo phase 2 predict
    for (i = 1; i < nr_rows - 2; i += 2)
    {
        row[i] += ip2_97 * (row[i - 1] + row[i + 1]);
    }
    row[nr_rows - 1] += 2 * ip2_97 * row[nr_rows - 2];

    // undo phase 1 update
    for (i = 2; i < nr_rows; i += 2)
    {
        row[i] += iu1_97 * (row[i - 1] + row[i + 1]);
    }
    row[0] += 2 * iu1_97 * row[1];

    // undo phase 1 predict
    for (i = 1; i < nr_rows - 2; i += 2)
    {
        row[i] += ip1_97 * (row[i - 1] + row[i + 1]);
    }
    row[nr_rows - 1] += 2 * ip1_97 * row[nr_rows - 2];
}

/*
* Perform in-situ transpose
*/
static void transpose(
    int w, /*int h,*/ // w == h
    uint8_t *src, int src_linesize)
{
    int i, j;
    uint8_t holder;
    for (i = 0; i < w - 2; ++i)
    {
        for (j = i + 1; j < w - 1; ++j)
        {
            holder = src[i * src_linesize + j];
            src[i * src_linesize + j] = src[j * src_linesize + i];
            src[j * src_linesize + i] = holder;
        }
    }
}

static void fast_wavelet_transform(
    int w,
    uint8_t *src, int src_linesize)
{
    // perform the separable fwt 9/7
    int nrows, prow;
    for (nrows = w; nrows >= RMIN; nrows >>= 1) // >> always half of the image
    {
        for (prow = 0; prow < nrows; prow++) {
            fast_wav_tr97(&src[prow * src_linesize], nrows);
        }
        transpose(w, src, src_linesize);
        for (prow = 0; prow < nrows; prow++) {
            fast_wav_tr97(&src[prow * src_linesize], nrows);
        }
        transpose(w, src, src_linesize);
    }
}

static void inverse_wavelet_transform(
    int w,
    uint8_t *src, int src_linesize)
{
    // perform the separable fwt 9/7
    int nrows, prow;
    for (nrows = RMIN; nrows <= w; nrows <<= 1) // >> always half of the image
    {
        for (prow = 0; prow < nrows; prow++) {
            iwd_wav_tr97(&src[prow * src_linesize], nrows);
        }
        transpose(w, src, src_linesize);
        for (prow = 0; prow < nrows; prow++) {
            iwd_wav_tr97(&src[prow * src_linesize], nrows);
        }
        transpose(w, src, src_linesize);
    }
}

static AVFrame *alloc_frame(enum AVPixelFormat pixfmt, int w, int h)
{
    AVFrame *frame = av_frame_alloc();
    if (!frame)
        return NULL;

    frame->format = pixfmt;
    frame->width  = w;
    frame->height = h;

    if (av_frame_get_buffer(frame, 32) < 0) {
        av_frame_free(&frame);
        return NULL;
    }
    return frame;
}


// apply threshold
// decide between hard - "keep or kill"
// or soft - "shrink or kill"
// - using >>soft<< as mostly preffered by community
static void apply_thr(
    int w,
    uint8_t *src, double thr,
    int linesize) 
{
    double first_val;
    int i,j;
    first_val = src[0];
    for (i = 0; i < w; i++) // w == h
    {
        for (j = 0; j < w; j++)
        {
            if (src[i*linesize + j] >= thr) {
                src[i*linesize + j] -= thr;
            }
            else if (src[i*linesize + j] <= -thr)
                src[i*linesize + j] += thr;
            else {
                src[i*linesize + j] = 0;
            }
        }
    }
    src[(w-1)*linesize + (w-1)] = first_val;
}


// copy roi from processed frame to output frame so no padding is on output
static void copy_roi_out(
    int src_w, uint8_t *src, int src_linesize, 
    int dst_w, int dst_h, uint8_t *dst, int dst_linesize)
{
    int nrows;
    for (nrows = 0; nrows <= dst_h; nrows++) 
    {
        memcpy(&dst[nrows * dst_linesize], &src[nrows * src_linesize], dst_w); 
    }
}


static int filter_frame(AVFilterLink *inlink, AVFrame *in)
{
    AVFilterContext *ctx = inlink->dst;
    WaveletDenoiseContext *waveletdnoise = ctx->priv;
    AVFilterLink *outlink = ctx->outputs[0];
    AVFrame *out, *in_padded;
    int p, orig_w, orig_h, log_w_h;
    double thr;
    orig_w = inlink->w;
    orig_h = inlink->h;
    if (av_frame_is_writable(in)) {
        out = in;
    } else {
        out = ff_get_video_buffer(outlink, outlink->w, outlink->h);
        if (!out) {
            av_frame_free(&in);
            return AVERROR(ENOMEM);
        }
        av_frame_copy_props(out, in);
    }
    log_w_h = find_size_to_fit_log(inlink->w, inlink->h);
    if (log_w_h == -1) {
        in_padded = in; // pass just the pointer
    } else {
        in_padded = alloc_frame(in->format, log_w_h, log_w_h); // allocate new frame with default vals
        if (!in_padded)
            return NULL;
        //copy the frame???
        av_frame_copy(in_padded, in);
    }
    /* waveletdnoise */
    for (p = 0; p < waveletdnoise->nb_planes; p++) { // waveletdnoise
        fast_wavelet_transform(in_padded->width, in_padded->data[p], in_padded->linesize[p]);
        thr = estimate_thr(in_padded->width, in_padded->data[p], in_padded->linesize[p], orig_h, orig_w);
        apply_thr(in_padded->width, in_padded->data[p], thr, in_padded->linesize[p]); // using soft
        inverse_wavelet_transform(in_padded->width, in_padded->data[p], in_padded->linesize[p]);
        // copy ROI from image padded to otput
        if(log_w_h != -1){
            copy_roi_out(in_padded->width, in_padded->data[p], in_padded->linesize[p], out->width, out->height, out->data[p], out->linesize[p]);
        }else{
            av_frame_copy(out, in_padded); // maybe just pass the pointer? ... this case will be rare
        }
        
    }
    free(tbank);
    free(coeffs);
    // copy the selection of the ROi before free
    av_frame_free(&in_padded);
    if (out != in)
        av_frame_free(&in);
    return ff_filter_frame(outlink, out);
}

static const AVFilterPad waveletdnoise_inputs[] = {
    {
        .name         = "default",
        .type         = AVMEDIA_TYPE_VIDEO,
        .config_props = config_props,
        .filter_frame = filter_frame,
    },
    { NULL }
};

static const AVFilterPad waveletdnoise_outputs[] = {
    {
        .name = "default",
        .type = AVMEDIA_TYPE_VIDEO,
    },
    { NULL }
};

AVFilter ff_vf_waveletdnoise = {
    .name          = "waveletdnoise",
    .description   = NULL_IF_CONFIG_SMALL("Create waveletdnoised video."),
    .priv_size     = sizeof(WaveletDenoiseContext),
    .query_formats = query_formats,
    .inputs        = waveletdnoise_inputs,
    .outputs       = waveletdnoise_outputs,
    .priv_class    = &waveletdnoise_class,
    .flags         = AVFILTER_FLAG_SUPPORT_TIMELINE_GENERIC,
};
