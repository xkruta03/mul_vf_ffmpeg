/*
 * 2017 Martin Krutak xkruta03 (at) vutbr (dot) cz
 *
 * This file is part of FFmpeg.
 *
 * Disclaimer: pieces of code taken from libavfilter/vf_edgedetect.c
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * @file
 * Create pixelated video with user input of height and width
 *
 * 
 */

#include "libavutil/imgutils.h"
#include "libavutil/avassert.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "avfilter.h"
#include "formats.h"
#include "internal.h"
#include "video.h"

static const int N_CH = 3; // number of channels 

typedef struct {
    const AVClass *class;
    int nb_planes; // 0 in RGB24
    int pix_width;
    int pix_height;
} PixelateContext;

#define OFFSET(x) offsetof(PixelateContext, x)
#define FLAGS AV_OPT_FLAG_FILTERING_PARAM|AV_OPT_FLAG_VIDEO_PARAM
static const AVOption pixelate_options[] = {
    { "pw", "set width of the pixel",   OFFSET(pix_width), AV_OPT_TYPE_INT, {5}, 1, 50, FLAGS },
    { "ph", "set height of the pixel",  OFFSET(pix_height), AV_OPT_TYPE_INT, {5}, 1, 50, FLAGS },
    { NULL }
};

AVFILTER_DEFINE_CLASS(pixelate);

static int query_formats(AVFilterContext *ctx)
{
     static const enum AVPixelFormat pix_fmts[] = {
        AV_PIX_FMT_RGB24, AV_PIX_FMT_NONE
    };
    AVFilterFormats *fmts_list = ff_make_format_list(pix_fmts);
    if (!fmts_list)
        return AVERROR(ENOMEM);
    return ff_set_common_formats(ctx, fmts_list);
}

static int config_props(AVFilterLink *inlink)
{
    AVFilterContext *ctx = inlink->dst;
    PixelateContext *pixelate = ctx->priv;
    pixelate->nb_planes = av_pix_fmt_count_planes(inlink->format); // either 1 for pixelate input or 3 fro RGB24
    return 0;
}

static void to_pixelate( 
          AVFilterContext *ctx,
          int w, int h,
          uint8_t *dst, int dst_linesize,
    const uint8_t *src, int src_linesize,
    const int PH, const int PW)
{
    int i, j;    
    for (j = 0; j < h;) {
        int stoph = 0;
        int stopw = 0;
        uint8_t red = 0;
        uint8_t green = 0;
        uint8_t blue = 0;        
        for (i = 0; i < w;) {
            // get the pixel value to be copied
            blue  = src[i*N_CH]; 
            green = src[i*N_CH + 1];
            red   = src[i*N_CH + 2];
            // change pixels in the block            
            for (int ph = 0; ph < PH - stoph; ph++)
            {
                for (int pw = 0; pw < PW - stopw; pw++)
                {
                    dst[i*N_CH + ph * dst_linesize + pw*N_CH]     = blue;
                    dst[i*N_CH + ph * dst_linesize + pw*N_CH + 1] = green;
                    dst[i*N_CH + ph * dst_linesize + pw*N_CH + 2] = red;
                }                
            }
            //update index for the edges
            i += PW;
            if((i + PW) > w){
                stopw = i + PW - w + 1;
            }else{
                stopw = 0;
            }
        }
        j += PH;
        if((j + PH) > h){
            stoph = j + PH - h + 1;
        }else{
            stoph = 0;
        }
        dst += PH * dst_linesize;
        src += PH * src_linesize;
    }
}

static int filter_frame(AVFilterLink *inlink, AVFrame *in)
{
    AVFilterContext *ctx = inlink->dst;
    PixelateContext *pixelate = ctx->priv;
    AVFilterLink *outlink = ctx->outputs[0];
    AVFrame *out;
    int p;
    if (av_frame_is_writable(in)) {
        out = in;
    } else {
        out = ff_get_video_buffer(outlink, outlink->w, outlink->h);
        if (!out) {
            av_frame_free(&in);
            return AVERROR(ENOMEM);
        }
        av_frame_copy_props(out, in);
    }

    /* pixelate */
    for (p = 0; p < pixelate->nb_planes; p++) { // to pixelate
        to_pixelate(
            ctx, inlink->w, inlink->h, 
            out->data[p], out->linesize[p],
            in->data[p], in->linesize[p],
            pixelate->pix_height, pixelate->pix_width);
    }

    if (out != in)
        av_frame_free(&in);
    return ff_filter_frame(outlink, out);
}

static const AVFilterPad pixelate_inputs[] = {
    {
        .name         = "default",
        .type         = AVMEDIA_TYPE_VIDEO,
        .config_props = config_props,
        .filter_frame = filter_frame,
    },
    { NULL }
};

static const AVFilterPad pixelate_outputs[] = {
    {
        .name = "default",
        .type = AVMEDIA_TYPE_VIDEO,
    },
    { NULL }
};

AVFilter ff_vf_pixelate = {
    .name          = "pixelate",
    .description   = NULL_IF_CONFIG_SMALL("Create pixelated video."),
    .priv_size     = sizeof(PixelateContext),
    .query_formats = query_formats,
    .inputs        = pixelate_inputs,
    .outputs       = pixelate_outputs,
    .priv_class    = &pixelate_class,
    .flags         = AVFILTER_FLAG_SUPPORT_TIMELINE_GENERIC,
};
