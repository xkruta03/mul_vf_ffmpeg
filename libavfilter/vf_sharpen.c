/*
 * 2017 Martin Krutak xkruta03 (at) vutbr (dot) cz
 *
 * This file is part of FFmpeg.
 *
 * Disclaimer: pieces of code taken from libavfilter/vf_edgedetect.c
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * @file
 * Create sharpened video with discrete approximations to the Laplacian filter
 *
 * 
 */

#include "libavutil/imgutils.h"
#include "libavutil/avassert.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "avfilter.h"
#include "formats.h"
#include "internal.h"
#include "video.h"


static const int MAX_P_VAL = 255;

typedef struct {
    const AVClass *class;
    int nb_planes;
    float alpha;
} SharpenContext;

#define OFFSET(x) offsetof(SharpenContext, x)
#define FLAGS AV_OPT_FLAG_FILTERING_PARAM|AV_OPT_FLAG_VIDEO_PARAM
static const AVOption sharpen_options[] = {
    { "alpha", "set the alpha for the kernel",   OFFSET(alpha), AV_OPT_TYPE_FLOAT, {.dbl=0.2}, 0, 1, FLAGS },
    { NULL }
};

AVFILTER_DEFINE_CLASS(sharpen);

static int query_formats(AVFilterContext *ctx)
{
     static const enum AVPixelFormat pix_fmts[] = {
        AV_PIX_FMT_GBRP,  AV_PIX_FMT_GRAY8, AV_PIX_FMT_NONE
    };
    AVFilterFormats *fmts_list = ff_make_format_list(pix_fmts);
    if (!fmts_list)
        return AVERROR(ENOMEM);
    return ff_set_common_formats(ctx, fmts_list);
}

static int config_props(AVFilterLink *inlink)
{
    AVFilterContext *ctx = inlink->dst;
    SharpenContext *sharpen = ctx->priv;
    sharpen->nb_planes = av_pix_fmt_count_planes(inlink->format); // either 1 for sharpen input or 3 fro RGB24
    return 0;
}

static void to_sharpen( 
          AVFilterContext *ctx,
          int w, int h,
          uint8_t *dst, int dst_linesize,
    const uint8_t *src, int src_linesize,
          float alpha)
{
    int i, j;
    int pix_val;
    int new_pixel;
    memcpy(dst, src, w); dst += dst_linesize; src += src_linesize;
    for (j = 1; j < h - 1; j++) {
        dst[0] = src[0];        
        for (i = 1; i < w - 1; i++) {
            /* diff. laplacian - sharpen kernel */
            /*
            * 1  1  1
            * 1 -8  1
            * 1  1  1
            */
            pix_val =     src[-src_linesize + i-1]  +  src[-src_linesize + i] + src[-src_linesize + i + 1]
                        + src[i - 1]                - (src[i] << 3)           + src[i + 1]
                        + src[src_linesize + i -1]  +  src[src_linesize + i]  + src[src_linesize + i + 1];
            new_pixel = (int)(src[i] - alpha * pix_val);
            // crop the value of the pixel
            if (new_pixel > MAX_P_VAL)
            {
                new_pixel = MAX_P_VAL;
            }
            if (new_pixel < 0)
            {
                new_pixel = 0;
            }
            dst[i] = (uint8_t)new_pixel;

        }
        dst[i + 1] = src[i + 1];

        dst += dst_linesize;
        src += src_linesize;
    }
    memcpy(dst, src, w);
}

static int filter_frame(AVFilterLink *inlink, AVFrame *in)
{
    AVFilterContext *ctx = inlink->dst;
    SharpenContext *sharpen = ctx->priv;
    AVFilterLink *outlink = ctx->outputs[0];
    AVFrame *out;
    int p;
    if (av_frame_is_writable(in)) {
        out = in;
    } else {
        out = ff_get_video_buffer(outlink, outlink->w, outlink->h);
        if (!out) {
            av_frame_free(&in);
            return AVERROR(ENOMEM);
        }
        av_frame_copy_props(out, in);
    }

    /* sharpen */
    for (p = 0; p < sharpen->nb_planes; p++) { // sharpen
        to_sharpen(
            ctx, inlink->w, inlink->h, 
            out->data[p], out->linesize[p],
            in->data[p], in->linesize[p],
            sharpen->alpha);
    }

    if (out != in)
        av_frame_free(&in);
    return ff_filter_frame(outlink, out);
}

static const AVFilterPad sharpen_inputs[] = {
    {
        .name         = "default",
        .type         = AVMEDIA_TYPE_VIDEO,
        .config_props = config_props,
        .filter_frame = filter_frame,
    },
    { NULL }
};

static const AVFilterPad sharpen_outputs[] = {
    {
        .name = "default",
        .type = AVMEDIA_TYPE_VIDEO,
    },
    { NULL }
};

AVFilter ff_vf_sharpen = {
    .name          = "sharpen",
    .description   = NULL_IF_CONFIG_SMALL("Create sharpened video."),
    .priv_size     = sizeof(SharpenContext),
    .query_formats = query_formats,
    .inputs        = sharpen_inputs,
    .outputs       = sharpen_outputs,
    .priv_class    = &sharpen_class,
    .flags         = AVFILTER_FLAG_SUPPORT_TIMELINE_GENERIC,
};
