/*
 * 2017 Martin Krutak xkruta03 (at) vutbr (dot) cz
 *
 * This file is part of FFmpeg.
 *
 * Disclaimer: pieces of code taken from libavfilter/vf_edgedetect.c
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * @file
 * Gaussian blur with kernel 5x5
 *
 * 
 */

#include "libavutil/imgutils.h"
#include "libavutil/avassert.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "avfilter.h"
#include "formats.h"
#include "internal.h"
#include "video.h"

typedef struct {
    const AVClass *class;
    int nb_planes;
} GausblurContext;

static const AVOption gausblur_options[] = {
    { NULL }
};

AVFILTER_DEFINE_CLASS(gausblur);

static int query_formats(AVFilterContext *ctx)
{
     static const enum AVPixelFormat pix_fmts[] = {
        AV_PIX_FMT_GBRP, AV_PIX_FMT_GRAY8, AV_PIX_FMT_NONE
    };
    AVFilterFormats *fmts_list = ff_make_format_list(pix_fmts);
    if (!fmts_list)
        return AVERROR(ENOMEM);
    return ff_set_common_formats(ctx, fmts_list);
}

static int config_props(AVFilterLink *inlink)
{
    AVFilterContext *ctx = inlink->dst;
    GausblurContext *gausblur = ctx->priv;
    gausblur->nb_planes = av_pix_fmt_count_planes(inlink->format); // either 1 for grayscale input or 3 fro GBRP
    return 0;
}

static void gaussian_blur(
          AVFilterContext *ctx, 
          int w, int h,
          uint8_t *dst, int dst_linesize,
    const uint8_t *src, int src_linesize)
{
    int i, j;

    memcpy(dst, src, w); dst += dst_linesize; src += src_linesize;
    memcpy(dst, src, w); dst += dst_linesize; src += src_linesize;
    for (j = 2; j < h - 2; j++) {
        dst[0] = src[0];
        dst[1] = src[1];
        for (i = 2; i < w - 2; i++) {
            /* Gaussian mask of size 5x5 with sigma 1 */
            dst[i] = ((src[-2*src_linesize + i-2] + src[2*src_linesize + i-2]) 
                    + (src[-2*src_linesize + i-1] + src[2*src_linesize + i-1]) * 4
                    + (src[-2*src_linesize + i  ] + src[2*src_linesize + i  ]) * 7
                    + (src[-2*src_linesize + i+1] + src[2*src_linesize + i+1]) * 4
                    + (src[-2*src_linesize + i+2] + src[2*src_linesize + i+2]) 

                    + (src[  -src_linesize + i-2] + src[  src_linesize + i-2]) * 4
                    + (src[  -src_linesize + i-1] + src[  src_linesize + i-1]) * 16
                    + (src[  -src_linesize + i  ] + src[  src_linesize + i  ]) * 26
                    + (src[  -src_linesize + i+1] + src[  src_linesize + i+1]) * 16
                    + (src[  -src_linesize + i+2] + src[  src_linesize + i+2]) * 4

                    + src[i-2] * 7
                    + src[i-1] * 26
                    + src[i  ] * 41
                    + src[i+1] * 26
                    + src[i+2] * 7) / 273;
        }
        dst[i    ] = src[i    ];
        dst[i + 1] = src[i + 1];

        dst += dst_linesize;
        src += src_linesize;
    }
    memcpy(dst, src, w); dst += dst_linesize; src += src_linesize;
    memcpy(dst, src, w);
}

static int filter_frame(AVFilterLink *inlink, AVFrame *in)
{
    AVFilterContext *ctx = inlink->dst;
    GausblurContext *gausblur = ctx->priv;
    AVFilterLink *outlink = ctx->outputs[0];
    AVFrame *out;
    int p;
    if (av_frame_is_writable(in)) {
        out = in;
    } else {
        out = ff_get_video_buffer(outlink, outlink->w, outlink->h);
        if (!out) {
            av_frame_free(&in);
            return AVERROR(ENOMEM);
        }
        av_frame_copy_props(out, in);
    }

    for (p = 0; p < gausblur->nb_planes; p++) {

        /* call the gaussian blur */
        gaussian_blur(ctx, 
                      inlink->w, inlink->h, 
                      out->data[p], out->linesize[p],
                      in->data[p], in->linesize[p]);
    }

    if (out != in)
        av_frame_free(&in);
    return ff_filter_frame(outlink, out);
}

static const AVFilterPad gausblur_inputs[] = {
    {
        .name         = "default",
        .type         = AVMEDIA_TYPE_VIDEO,
        .config_props = config_props,
        .filter_frame = filter_frame,
    },
    { NULL }
};

static const AVFilterPad gausblur_outputs[] = {
    {
        .name = "default",
        .type = AVMEDIA_TYPE_VIDEO,
    },
    { NULL }
};

AVFilter ff_vf_gausblur = {
    .name          = "gausblur",
    .description   = NULL_IF_CONFIG_SMALL("Apply gaussian blur 5x5."),
    .priv_size     = sizeof(GausblurContext),
    .query_formats = query_formats,
    .inputs        = gausblur_inputs,
    .outputs       = gausblur_outputs,
    .priv_class    = &gausblur_class,
    .flags         = AVFILTER_FLAG_SUPPORT_TIMELINE_GENERIC,
};
