/*
 * 2017 Martin Krutak xkruta03 (at) vutbr (dot) cz
 *
 * This file is part of FFmpeg.
 *
 * Disclaimer: pieces of code taken from libavfilter/vf_edgedetect.c
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * @file
 * RGB to grayscale
 *
 * 
 */

#include "libavutil/imgutils.h"
#include "libavutil/avassert.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "avfilter.h"
#include "formats.h"
#include "internal.h"
#include "video.h"

static const float R_MULT = 0.299;
static const float G_MULT = 0.587;
static const float B_MULT = 0.114;
static const int N_CH     = 3; // number of channels

typedef struct {
    const AVClass *class;
    int nb_planes;
} GrayscaleContext;

static const AVOption grayscale_options[] = {
    { NULL }
};

AVFILTER_DEFINE_CLASS(grayscale);

static int query_formats(AVFilterContext *ctx)
{
     static const enum AVPixelFormat pix_fmts[] = {
        AV_PIX_FMT_RGB24, AV_PIX_FMT_NONE
    };
    AVFilterFormats *fmts_list = ff_make_format_list(pix_fmts);
    if (!fmts_list)
        return AVERROR(ENOMEM);
    return ff_set_common_formats(ctx, fmts_list);
}

static int config_props(AVFilterLink *inlink)
{
    AVFilterContext *ctx = inlink->dst;
    GrayscaleContext *grayscale = ctx->priv;
    grayscale->nb_planes = av_pix_fmt_count_planes(inlink->format); // either 1 for grayscale input or 3 fro RGB24
    return 0;
}

static void to_grayscale( 
          AVFilterContext *ctx,
          int w, int h,
          uint8_t *dst, int dst_linesize,
    const uint8_t *src, int src_linesize)
{
    int i, j;
    for (j = 0; j < h; j++) {
        float acc = 0;
        uint8_t red = 0;
        uint8_t green = 0;
        uint8_t blue = 0;        
        for (i = 0; i < w; i++) {
            blue  = src[i*N_CH];
            green = src[i*N_CH + 1];
            red   = src[i*N_CH + 2];

            acc   = R_MULT * red + G_MULT * green + B_MULT * blue;

            dst[i*N_CH]     = (uint8_t)acc;
            dst[i*N_CH + 1] = (uint8_t)acc;
            dst[i*N_CH + 2] = (uint8_t)acc;
        }
        dst += dst_linesize;
        src += src_linesize;
    }
}

static int filter_frame(AVFilterLink *inlink, AVFrame *in)
{
    AVFilterContext *ctx = inlink->dst;
    GrayscaleContext *grayscale = ctx->priv;
    AVFilterLink *outlink = ctx->outputs[0];
    AVFrame *out;
    int p;
    if (av_frame_is_writable(in)) {
        out = in;
    } else {
        out = ff_get_video_buffer(outlink, outlink->w, outlink->h);
        if (!out) {
            av_frame_free(&in);
            return AVERROR(ENOMEM);
        }
        av_frame_copy_props(out, in);
    }

        /* RGB to grayscale */
    for (p = 0; p < grayscale->nb_planes; p++) { // to grayscale
        to_grayscale(
            ctx, inlink->w, inlink->h, 
            out->data[p], out->linesize[p],
            in->data[p], in->linesize[p]);
    }

    if (out != in)
        av_frame_free(&in);
    return ff_filter_frame(outlink, out);
}

static const AVFilterPad grayscale_inputs[] = {
    {
        .name         = "default",
        .type         = AVMEDIA_TYPE_VIDEO,
        .config_props = config_props,
        .filter_frame = filter_frame,
    },
    { NULL }
};

static const AVFilterPad grayscale_outputs[] = {
    {
        .name = "default",
        .type = AVMEDIA_TYPE_VIDEO,
    },
    { NULL }
};

AVFilter ff_vf_grayscale = {
    .name          = "grayscale",
    .description   = NULL_IF_CONFIG_SMALL("Convert RGB to grayscale."),
    .priv_size     = sizeof(GrayscaleContext),
    .query_formats = query_formats,
    .inputs        = grayscale_inputs,
    .outputs       = grayscale_outputs,
    .priv_class    = &grayscale_class,
    .flags         = AVFILTER_FLAG_SUPPORT_TIMELINE_GENERIC,
};
